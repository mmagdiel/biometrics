<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language' => 'es-MX',
    'bootstrap' => ['log'],
    'defaultRoute' => 'site/index',
    'as access' => [
        'class' => '\hscstudio\mimin\components\AccessControl',
            'allowActions' => [
                'site/*',
                'debug/*',
                'mimin/*' // only in dev mode
        ],
    ],
    'modules' => [
        'report' => [
            'class' => 'app\modules\admin\Report',
        ],
        'upload' => [
            'class' => 'app\modules\upload\Upload',
        ],
        'mimin' => [
            'class' => '\hscstudio\mimin\Module',
        ],
        'rrhh' => [
            'class' => 'app\modules\rrhh\Rrhh',
        ],
        'user' => [
            'class' => 'app\modules\user\User',
        ],
    ],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'U_leGCN-rswXjdqNB8qwfGJAB5N2Scr4',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'formatter'=>[
            'class' => 'app\components\MyFormatter',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error'
        ],
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
