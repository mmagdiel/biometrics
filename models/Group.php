<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "group".
 *
 * @property string $id
 * @property string $name
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property Permission[] $permissions
 */
class Group extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group';
    }

    /**
     *  Public doc   
     */    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by', 'created_at', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 127],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermissions()
    {
        return $this->hasMany(Permission::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMembers() 
    { 
        return $this->hasMany(Member::className(), ['group_id' => 'id']); 
    } 
 
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getPeople() 
    { 
        return $this->hasMany(Person::className(), ['id' => 'person_id'])->viaTable('member', ['group_id' => 'id']); 
    } 

    /**
     * @inheritdoc
     * @return \app\models\query\GroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\GroupQuery(get_called_class());
    }
}
