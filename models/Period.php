<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "period".
 *
 * @property string $id
 * @property string $month
 * @property integer $year
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property Summary[] $summaries
 * @property Person[] $people
 */
class Period extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'period';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['month', 'year'], 'required'],
            [['year', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['month'], 'string', 'max' => 17],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'month' => Yii::t('app', 'Month'),
            'year' => Yii::t('app', 'Year'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSummaries()
    {
        return $this->hasMany(Summary::className(), ['period_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasMany(Person::className(), ['id' => 'person_id'])->viaTable('summary', ['period_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\query\PeriodQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\PeriodQuery(get_called_class());
    }
}
