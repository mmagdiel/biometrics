<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "summary".
 *
 * @property integer $monthly
 * @property integer $fortnight1
 * @property integer $fortnight2
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 * @property string $person_id
 * @property string $period_id
 *
 * @property Period $period
 * @property Person $person
 */
class Summary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'summary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['monthly', 'fortnight1', 'fortnight2', 'person_id', 'period_id'], 'required'],
            [['monthly', 'fortnight1', 'fortnight2', 'created_at', 'created_by', 'updated_at', 'updated_by', 'person_id', 'period_id'], 'integer'],
            [['period_id'], 'exist', 'skipOnError' => true, 'targetClass' => Period::className(), 'targetAttribute' => ['period_id' => 'id']],
            [['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::className(), 'targetAttribute' => ['person_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'monthly' => Yii::t('app', 'Monthly'),
            'fortnight1' => Yii::t('app', 'Fortnight1'),
            'fortnight2' => Yii::t('app', 'Fortnight2'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'person_id' => Yii::t('app', 'Person ID'),
            'period_id' => Yii::t('app', 'Period ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriod()
    {
        return $this->hasOne(Period::className(), ['id' => 'period_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Person::className(), ['id' => 'person_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\query\SummaryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\SummaryQuery(get_called_class());
    }
}
