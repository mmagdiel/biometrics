<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Date]].
 *
 * @see \app\models\Date
 */
class DateQuery extends \yii\db\ActiveQuery
{
    /**
     *
     */
    public function year($year)
    {
        return $this->andWhere(['=', 'year', $year]);
    }

    /**
     *
     */
    public function month($month)
    {
        return $this->andWhere(['=', 'month', $month]);
    }

    /**
     * 
     */
    public function fortnight1()
    {
        return $this->andWhere(['<=', 'number_day', 15]);
    }

    /**
     * 
     */
    public function fortnight2()
    {
        return $this->andWhere(['>=', 'number_day', 15]);
    }

    /**
     * @inheritdoc
     * @return \app\models\Date[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\Date|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
