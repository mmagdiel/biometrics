<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Event]].
 *
 * @see \app\models\Event
 */
class EventQuery extends \yii\db\ActiveQuery
{
    /**
     *
     */
    public function year($year)
    {
        return $this->andWhere(['=', 'year', $year]);
    }

    /**
     *
     */
    public function number($number)
    {
        return $this->andWhere(['=', 'number_years_day', $number]);
    }

    /**
     *
     */
    public function person($person)
    {
        return $this->andWhere(['=', 'person_id', $person]);
    }

    /**
     * @inheritdoc
     * @return \app\models\Event[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\Event|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
