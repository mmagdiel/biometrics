<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Period]].
 *
 * @see \app\models\Period
 */
class PeriodQuery extends \yii\db\ActiveQuery
{
    /**
     *
     */
    public function year($year)
    {
        return $this->andWhere(['year' => $year]);
    }

    /**
     *
     */
    public function month($month)
    {
        return $this->andWhere(['month' => $month]);
    }

    /**
     * @inheritdoc
     * @return \app\models\Period[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\Period|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
