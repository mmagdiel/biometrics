<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Record]].
 *
 * @see \app\models\Record
 */
class RecordQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     *
     */
    public function person($person)
    {
        return $this->andWhere(['=', 'person_id', $person]);
    }

    /**
     *
     */
    public function date($date)
    {
        return $this->andWhere(['=', 'date_id', $date]);
    }

    /**
     * @inheritdoc
     * @return \app\models\Record[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\Record|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
