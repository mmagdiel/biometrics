<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Summary]].
 *
 * @see \app\models\Summary
 */
class SummaryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\models\Summary[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\Summary|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
