<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Record;
use app\models\Permission;
use app\models\User;

/**
 * RecordSearch represents the model behind the search form about `app\models\Record`.
 */
class RecordsSearch extends Record
{
    public $person;
    public $month;
    public $year;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['month'], 'string'],
            [['person', 'year'], 'integer'],
            [['person', 'year', 'month'], 'safe'],
            [['person', 'year', 'month'], 'required'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {    
        $query = Record::find();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 29,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'person_id' => $this->person,
        ]);
        
        $query->joinWith('date');

        $query->andFilterWhere(['=', 'person_id', $this->person])
              ->andFilterWhere(['=','date.month',$this->month])
              ->andFilterWhere(['=','date.year',$this->year]);
        
        return $dataProvider;
    }
}
