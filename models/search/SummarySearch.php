<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Summary;

/**
 * SummarySearch represents the model behind the search form about `app\models\Summary`.
 */
class SummarySearch extends Summary
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['monthly', 'fortnight1', 'fortnight2', 'created_at', 'created_by', 'updated_at', 'updated_by', 'person_id', 'period_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Summary::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'monthly' => $this->monthly,
            'fortnight1' => $this->fortnight1,
            'fortnight2' => $this->fortnight2,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'person_id' => $this->person_id,
            'period_id' => $this->period_id,
        ]);

        return $dataProvider;
    }
}
