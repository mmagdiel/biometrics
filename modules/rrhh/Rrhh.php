<?php

namespace app\modules\rrhh;

/**
 * rrhh module definition class
 */
class Rrhh extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\rrhh\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
