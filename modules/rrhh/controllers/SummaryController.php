<?php

namespace app\modules\rrhh\controllers;

use Yii;
use app\models\Summary;
use app\models\search\SummarySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SummaryController implements the CRUD actions for Summary model.
 */
class SummaryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Summary models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SummarySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Summary model.
     * @param string $person_id
     * @param string $period_id
     * @return mixed
     */
    public function actionView($person_id, $period_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($person_id, $period_id),
        ]);
    }

    /**
     * Finds the Summary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $person_id
     * @param string $period_id
     * @return Summary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($person_id, $period_id)
    {
        if (($model = Summary::findOne(['person_id' => $person_id, 'period_id' => $period_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
