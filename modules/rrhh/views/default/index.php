<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\User;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\RecordSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Records');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="record-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', [
        'model' => $searchModel, 
        'people' => $people,
        'months' => $months,
        'years' => $years
    ]); ?>

<?php Pjax::begin(); ?>    <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'rowOptions' => function($model){
        if($model->time_worked >= 28800){
            return ['class' => 'success'];
        }else{
            return ['class' => 'danger'];
        }
    },
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'person.name',
            'value' => 'person.name',
        ],
        'date.weekday',
        'date.number_day',
        'date.month',
        'date.year',
        'counter_record',
        'time_record:duration',
        'counter_worked',
        'time_worked:duration',
        [
            'class' => 'yii\grid\ActionColumn',
            'visibleButtons' => [ 
                'update' => False, 
                'delete' => False
                ]
        ],
    ],
]); ?>
<?php Pjax::end(); ?></div>

    <h1><?= Yii::t('app', 'Summary') ?></h1>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'fortnight1:durations',
        'fortnight2:durations',
        'monthly:durations',
    ],
]);
?>
