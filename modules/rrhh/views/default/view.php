<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\DetailView;
use app\models\User;
/* @var $this yii\web\View */
/* @var $model app\models\Record */

$this->title = $model->person_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Records'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="record-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'person.name',
            'date.weekday',
            'date.number_day',
            'date.month',
            'date.year',
            'counter_record',
            'time_record:durations',
            'counter_worked',
            'time_worked:durations',
            'min_record:datetime',
            'max_record:datetime',
            'average_record:datetime',
/*
            [
                'attribute'=>'Created_at',
                'value'=>function($dataProvider){
                    return Yii::$app->formatter->asDatetime($dataProvider->created_at, 'short');
                },
            ],
            [
                'attribute'=>'Created_by',
                'value'=>function($dataProvider){
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            [
                'attribute'=>'Updated_at',
                'value'=>function($dataProvider){
                    return Yii::$app->formatter->asDatetime($dataProvider->updated_at, 'short');
                },
            ],
            [
                'attribute'=>'Updated_by',
                'value'=>function($dataProvider){
                    return User::findIdentity($dataProvider->updated_by)->username;
                },
            ],
*/
        ],
    ]) ?> </div>

<h1><?= Yii::t('app','Workeds') ?></h1>

<div><?php Pjax::begin(); ?> <?= GridView::widget([
        'dataProvider' => $datasProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'person.name',
            [
                'attribute'=>'In',
                'value'=>function($dataProvider){
                    return Yii::$app->formatter->asDatetime($dataProvider->in, 'short');
                },
            ],
            [
                'attribute'=>'Out',
                'value'=>function($dataProvider){
                    return Yii::$app->formatter->asDatetime($dataProvider->out, 'short');
                },
            ],
            'date.number_day',
            'date.weekday',
            'date.month',
            'date.year',
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<h1><?= Yii::t('app','Events') ?></h1>

<div><?php Pjax::begin(); ?> <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'person.name',
            [
                'attribute'=>'Time',
                'value'=>function($dataProvider){
                    return Yii::$app->formatter->asDatetime($dataProvider->unix_time, 'short');
                },
            ],
            [
                'attribute'=>'Event',
                'value'=>function($dataProvider){
                    return $dataProvider->event == '192.168.10.15' ? 'Salida' : 'Entrada';
                }
            ],
            'number_years_day',
            'year',
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

