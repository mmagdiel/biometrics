<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SummarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Summaries');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="summary-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Summary'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'monthly:durations',
            'fortnight1:durations',
            'fortnight2:durations',
            [
                'attribute' => 'person.name',
                'value' => 'person.name',
            ],
            [
                'attribute' => 'period.month',
                'value' => 'period.month',
            ],
            [
                'attribute' => 'period.year',
                'value' => 'period.year',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [  
                    'delete' => False,
                    'update' => False
                    ]
            ]
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
