<?php

namespace app\modules\upload\controllers;

use Yii;
use app\models\File;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use app\components\helpers\MyQuery;
use app\models\search\FileSearch;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\UnsupportedMediaTypeHttpException;

/**
 * Default controller for the `upload` module
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all File models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            throw new UnauthorizedHttpException("Error!!!!");
        }

        $searchModel = new FileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single File model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {
            throw new UnauthorizedHttpException("Error!!!!");
        }
        $data = MyQuery::count('data','file_id',$id);
        $person = MyQuery::count('person','file_id',$id);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'data' => $data,
            'person' => $person
            ]);
    }

    /**
     * Creates a new File model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            throw new UnauthorizedHttpException("Error!!!!");
        }

        $model = new File();
        $this->handle($model);

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the File model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return File the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = File::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Upload file to the system
     */ 
    protected function handle(File $model)
    {
        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model,'file');
            if (!isset($model->file)) {
                throw new NotFoundHttpException("Error Processing the File");
            }
            $ext = $model->file->extension;
            print("<pre>".print_r($ext,true)."</pre>");
            if(strcmp($ext,'xls') != 0 || !strcmp($ext,'xlsx') != 0) {
                throw new UnsupportedMediaTypeHttpException("Error Processing Request");
            }
            $token = Yii::$app->security->generateRandomString(23);
            $model->path = 'excel/' . $token . '.' . $ext;
            $model->file->saveAs($model->path);
            $model->error = $model->file->error;
            if ($model->error != '0') {
                throw new NotFoundHttpException("Error Saved the File");
            }
            $model->size = $model->file->size;
            $model->name = $model->file->name;
            if ($model->save(false)) {
                $id = $model->id;
                $user = Yii::$app->get('user', false);
                $user = $user && !$user->isGuest ? $user->id : 0;
                // Ojo: en la base de dato el Usurio 0 es por defecto, no debe de existir otro usuario real con ese id
                $runner = new \tebazil\runner\ConsoleCommandRunner();
                $runner->run('upload/data', [$id, $user]);
                $output = $runner->getOutput();
                $exitCode = $runner->getExitCode();
                return $this->redirect(['view', 
                   'id' => $id
                ]);
            }
        }
    }
}
