<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Data */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'time',
            'number',
            'name',
            [
                'attribute' => 'Event',
                'value' => function($model){
                    return $model->event == '192.168.10.15' ? 'Salida' : 'Entrada';
                }
            ],
            'created_at:datetime',
            [
                'attribute' => 'Created_by',
                'value' => function($model){
                    return User::findIdentity($model->created_by)->username;
                },
            ],
            'updated_at:datetime',
            [
                'attribute' => 'Updated_by',
                'value' => function($model){
                    return User::findIdentity($model->updated_by)->username;
                },
            ],
        ],
    ]) ?>

</div>
