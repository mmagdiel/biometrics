<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Person;
use app\models\Group;

/* @var $this yii\web\View */
/* @var $model app\models\Member */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="member-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'person_id')->dropDownList(
        ArrayHelper::map(Person::find()->where(['visible' => 1])->all(),'id', 'name'),
        ['prompt'=>'Seleccione una categoria']) 
     ?>

    <?= $form->field($model, 'group_id')->dropDownList(
        ArrayHelper::map(Group::find()->all(),'id', 'name'),
        ['prompt'=>'Seleccione una categoria']) 
     ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
