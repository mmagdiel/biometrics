<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Group;
use app\models\Person;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Permission */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="permission-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'person_id')->dropDownList(
        ArrayHelper::map(Person::username(),'id', 'ci'),
        ['prompt'=>'Seleccione una region'])  
    ?>

    <?= $form->field($model, 'user_id')->dropDownList(
        ArrayHelper::map(User::find()->where('ci>0')->all(),'id', 'ci'),
        ['prompt'=>'Seleccione un usuario'])  
    ?>
        
    <?= $form->field($model, 'responsable')->checkbox()  ?>

    <?= $form->field($model, 'group_id')->dropDownList(
        ArrayHelper::map(Group::find()->all(),'id', 'name'),
        ['prompt'=>'Seleccione un grupo'])
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
