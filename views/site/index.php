<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="site-index">

    <div class="jumbotron">
        <?= Html::img('@web/img/logo.jpg', ['alt'=>Yii::$app->name, 'height' => '257', 'width' => '257']); ?>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-md-3">
                <h3>Glosario</h3>
                    <p class="text-justify">Registros: Son eventos de entrada o salida capturados y almacenados por el biometrico.</p>
                    <p class="text-justify">Trabajado: Par de entrada y salida continuas, en ese mismo orden.</p>
                    <p class="text-justify">Registros aislados: Registros que no forman pares de entrada salida. Ejemplo: un único registro en la jornada.</p>
            </div>
            <div class="col-md-3">
                <h3><?= Yii::t('app', 'Start')?></h3>
                    <p class="text-justify">Al registrarse notificar al correo coordinador.premieresi@gmail.com para habilitar los permisos requeridos.</p>
                    <h4 class="alert alert-danger">Los registros aislado no formaran parte de evaluación</h4>
            </div>                    
            <div class="col-md-3">
                <h3> Reporte </h3>
                    <p class="text-justify">El reporte interativo le permitira escoger a la persona, el mes y el año.</p>
                    <p class="text-justify">En el caso de superar las 8h diarias de trabajo en esa jornada se indicará de color verde</p>
                    <p class="text-justify">Para visualizar un reporte más detallado de la jornada laboral dar click en icono del ojo.</p>
                    <p class="text-justify">Al final del reporte, se muestra un resumen de las horas trabajadas tanto en en cada quincena como el mes</p> 
            </div>
            <div class="col-md-3">
                <h3>Importante</h3>
                    <p class="text-justify">Para la evaluación general se tomará el tiempo trabajado.</p>
                    <p class="text-justify"> El cual depende de una entrada y una salida posterior.</p>
                    <h4 class="alert alert-info text-justify">No olvide marcar su entrada y la salida en el biometrico.</h4>
            </div>
        </div>
    </div>
</div>
